package com.example.jetpackcomposemvvmapp

import com.example.jetpackcomposemvvmapp.ui.contacts.Results
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("api/1.0")
    suspend fun getContacts(@Query("seed")seed: String, @Query("results")results: Int,
                            @Query("page") page: Int) : Results

    companion object {
        var apiService: ApiService? = null
        fun getInstance() : ApiService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl("https://randomuser.me/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(ApiService::class.java)
            }
            return apiService!!
        }
    }
}