package com.example.jetpackcomposemvvmapp.ui.contacts.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.jetpackcomposemvvmapp.ui.contacts.Contact
import com.example.jetpackcomposemvvmapp.ui.contacts.ContactViewModel
import com.example.jetpackcomposemvvmapp.ui.theme.JetpackComposeMVVMAppTheme

class ContactDetailsFragment : Fragment() {
    private val contactViewModel by viewModels<ContactViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val contact = arguments?.getString("contactEmail")?.let { contactEmail ->
        contactViewModel.getContact(contactEmail)
    }
        return ComposeView(requireContext()).apply {
            setContent {
                JetpackComposeMVVMAppTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(color = MaterialTheme.colors.background) {
                        contact?.let { ContactDetailsItem(it) }
                    }
                }
            }
        }
    }

    @Composable
    fun ContactDetailsItem(contact: Contact) {

        Column {
            DetailsProperty(contact.gender)
            DetailsProperty(contact.nat)
            DetailsProperty(contact.cell)
        }
    }
    @Composable
    fun DetailsProperty(label: String) {
        Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
            Divider(modifier = Modifier.padding(bottom = 4.dp))
            Text(
                text = label,
                modifier = Modifier.height(24.dp),
                style = MaterialTheme.typography.caption,
            )
        }
    }
}
