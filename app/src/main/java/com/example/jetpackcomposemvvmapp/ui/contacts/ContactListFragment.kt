package com.example.jetpackcomposemvvmapp.ui.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation.findNavController
import com.example.jetpackcomposemvvmapp.R
import com.example.jetpackcomposemvvmapp.ui.theme.JetpackComposeMVVMAppTheme


class ContactListFragment: Fragment() {
    private val contactViewModel by viewModels<ContactViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                JetpackComposeMVVMAppTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(color = MaterialTheme.colors.background) {
                        ContactList(contactList = ContactViewModel.contactListResponse, this)
                        contactViewModel.getContactList(seed = "lydia", results = 84, page = 42)
                    }
                }
            }


        }
    }

    @Composable
    fun ContactList(contactList: List<Contact>, composeView: ComposeView) {
        var selectedIndex by remember { mutableStateOf(-1) }
        LazyColumn {

            itemsIndexed(items = contactList, ) { index, item ->
                ContactItem(contact = item, index, selectedIndex, composeView) { i ->
                    selectedIndex = i
                }
            }
        }
    }

    @Composable
    fun ContactItem(contact: Contact, index: Int, selectedIndex: Int, view: ComposeView , onClick: (Int) -> Unit) {
        val backgroundColor =
            if (index == selectedIndex) MaterialTheme.colors.primary else MaterialTheme.colors.background
        Card(
            modifier = Modifier
                .padding(6.dp, 4.dp)
                .fillMaxWidth()
                .clickable( onClick = {
                    contactViewModel.getContact(contact.email)
                    val bundle = Bundle()
                    bundle.putString("contactEmail", contact.email)
                    findNavController( view.rootView.findViewById(R.id.main_nav_host_fragment)).navigate(R.id.contactDetailsFragment, bundle) } )
                .height(80.dp), shape = RoundedCornerShape(8.dp), elevation = 4.dp
        ) {
            Surface(color = backgroundColor) {

                Row(
                    Modifier
                        .padding(8.dp)
                        .fillMaxSize()
                ) {
                    Text(
                        text = contact.name.first,
                        style = MaterialTheme.typography.subtitle1,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .padding(0.dp, 8.dp)
                    )
                    Text(
                        text = contact.name.last,
                        style = MaterialTheme.typography.h5,

                        modifier = Modifier
                            .padding(10.dp, 0.dp)
                    )
                    Column(
                        verticalArrangement = Arrangement.Bottom,
                        horizontalAlignment = Alignment.End,
                        modifier = Modifier
                            .padding(4.dp)
                            .fillMaxHeight()
                            .weight(0.9f)
                    ) {

                        Text(
                            text = contact.email,
                            style = MaterialTheme.typography.caption,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier
                                .background(
                                    Color.LightGray)
                                .padding(4.dp)
                        )

                    }
                }
            }
        }
    }

}