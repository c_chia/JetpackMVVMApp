package com.example.jetpackcomposemvvmapp.ui.contacts

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jetpackcomposemvvmapp.ApiService
import kotlinx.coroutines.launch

class ContactViewModel: ViewModel() {

    companion object{ var contactListResponse: List<Contact> by mutableStateOf(listOf()) }
    private var errorMessage: String by mutableStateOf("")


    fun getContactList(seed: String, results: Int, page: Int){
        viewModelScope.launch {
            val apiService = ApiService.getInstance()
            try{
                val contactList = apiService.getContacts(seed, results, page).results
                contactListResponse = contactList
            }
            catch (e: Exception){
                errorMessage = e.message.toString()
            }
        }
    }

    fun getContact(email: String): Contact? {
             return contactListResponse.find{ it.email == email
        }
    }
}